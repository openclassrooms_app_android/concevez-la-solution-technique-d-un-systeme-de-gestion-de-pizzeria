
CREATE TABLE ocp_pizzeria (
                id BIGINT AUTO_INCREMENT NOT NULL,
                name VARCHAR(100) NOT NULL,
                address VARCHAR(150) NOT NULL,
                zip_code VARCHAR(5) NOT NULL,
                city VARCHAR(50) NOT NULL,
                phone_number VARCHAR(10) NOT NULL,
                created_at DATETIME NOT NULL,
                updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE ocp_ingredients (
                id BIGINT AUTO_INCREMENT NOT NULL,
                name VARCHAR(100) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE ocp_stocks (
                id BIGINT AUTO_INCREMENT NOT NULL,
                ingredient_id BIGINT NOT NULL,
                quantity SMALLINT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE ocp_pizzas (
                id BIGINT AUTO_INCREMENT NOT NULL,
                photo_url VARCHAR(200) NOT NULL,
                name VARCHAR(50) NOT NULL,
                price DOUBLE PRECISION NOT NULL,
                created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE ocp_pizzas_ingredients (
                id BIGINT NOT NULL,
                pizza_id BIGINT NOT NULL,
                ingredient_id BIGINT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE ocp_pizzas_ordered (
                id BIGINT NOT NULL,
                order_id VARCHAR(10) NOT NULL,
                pizza_id BIGINT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE ocp_roles (
                id BIGINT AUTO_INCREMENT NOT NULL,
                name VARCHAR(30) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE ocp_status (
                id BIGINT AUTO_INCREMENT NOT NULL,
                name VARCHAR(50) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE ocp_users (
                id BIGINT NOT NULL,
                firstname VARCHAR(50) NOT NULL,
                lastname VARCHAR(50) NOT NULL,
                address VARCHAR(150) NOT NULL,
                zip_code VARCHAR(5) NOT NULL,
                city VARCHAR(50) NOT NULL,
                email VARCHAR(100) NOT NULL,
                phone_number VARCHAR(10) NOT NULL,
                role_id BIGINT NOT NULL,
                created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE ocp_orders (
                id BIGINT NOT NULL,
                order_id VARCHAR(10) NOT NULL,
                status_id BIGINT NOT NULL,
                user_id BIGINT NOT NULL,
                pizzeria_id BIGINT NOT NULL,
                created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                PRIMARY KEY (id)
);


ALTER TABLE ocp_orders ADD CONSTRAINT ocp_pizzeria_ocp_orders_fk
FOREIGN KEY (pizzeria_id)
REFERENCES ocp_pizzeria (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ocp_stocks ADD CONSTRAINT ocp_ingredients_ocp_stocks_fk
FOREIGN KEY (ingredient_id)
REFERENCES ocp_ingredients (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ocp_pizzas_ingredients ADD CONSTRAINT ocp_ingredients_ocp_pizzas_ingredients_fk
FOREIGN KEY (ingredient_id)
REFERENCES ocp_ingredients (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ocp_pizzas_ordered ADD CONSTRAINT ocp_pizzas_ocp_pizzas_ordered_fk
FOREIGN KEY (pizza_id)
REFERENCES ocp_pizzas (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ocp_pizzas_ingredients ADD CONSTRAINT ocp_pizzas_ocp_pizzas_ingredients_fk
FOREIGN KEY (pizza_id)
REFERENCES ocp_pizzas (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ocp_users ADD CONSTRAINT ocp_roles_ocp_users_fk
FOREIGN KEY (role_id)
REFERENCES ocp_roles (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ocp_orders ADD CONSTRAINT ocp_status_ocp_orders_fk
FOREIGN KEY (status_id)
REFERENCES ocp_status (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ocp_orders ADD CONSTRAINT ocp_users_ocp_orders_fk
FOREIGN KEY (user_id)
REFERENCES ocp_users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
